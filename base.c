#include <stdio.h>
#include <stdlib.h>
#include "estudiantes.h"
#include <math.h>

int const_array = 24;// Lista de arreglos con la cantidad de alumnos
int validar_carga=0;// Variable para saber si se cargo el archivo
float desvStd(float media, float prom[]){//Funcion para crear la desviacion estandar
	float desvstd;
	float varianza = 0.0;
	int i=0;
	desvstd=calcularVarianza(media, prom);// Calculo necesario para la desviacion estandar
	return sqrtf(desvstd);
	while(i<const_array){
		varianza = varianza + (pow((prom[i]-media), 2));
		i++;
	}
	return(varianza/((float)const_array-1));
}
float menor(int i, float nota_min, float prom[]){// Busca el menor promedio de los alumnos
	if(prom[i]<nota_min){
		return prom[i];
	}
	else{
		return nota_min;
	}
}
float mayor(int i, float nota_max, float prom[]){// Busca el mayor promedio de los alumnos
	if(prom[i]>nota_max){
		return prom[i];
	}
	else{
		return nota_max;
	}
}


void registroCurso(estudiante curso[]){//Se ingresan las notas de los alumnos para evaluar y guardar en programa
	int i;
	float promX, promC;
	for (i = 0; i<5; i++)
	{
		printf("\nAlumno: %s %s %s\n", curso[i].nombre,curso[i].apellidoP,
			curso[i].apellidoM);
		printf("Ingrese las notas de cada proyecto: \n");
		scanf("%f %f %f %f %f %f", &curso[i].asig_1.cont1, &curso[i].asig_1.cont2, 
			&curso[i].asig_1.cont3, &curso[i].asig_1.cont4, &curso[i].asig_1.cont5, 
			&curso[i].asig_1.cont6);
		promX = (curso[i].asig_1.proy1 + curso[i].asig_1.proy2 + curso[i].asig_1.proy3)/3;
		promC = (curso[i].asig_1.cont1 + curso[i].asig_1.cont2 + curso[i].asig_1.cont3 + curso[i].asig_1.cont4 + curso[i].asig_1.cont5 + curso[i].asig_1.cont6)/6;
		curso[i].prom = (promX + promC)/2;
	}
}
void metricasEstudiantes(estudiante curso[])//Funcion donde muestra por pantalla los resultados de la funcion desviacion estandar
{
	if(validar_carga == 1){
		int i=0;
		float prom[const_array], nota_min=7.0, nota_max= 1.0, media=0.0, desvstd;
		while(i< const_array){
			prom[i]= curso[i].prom;
			nota_min = menor(i, nota_min, prom);
			nota_max= mayor(i, nota_max, prom);
			media= media + prom[i];
			i++;
		}
		media= (media/const_array);
		desvstd=desvStd(media, prom);
		printf("\nNota mayor: %.1f\nNota menor: %.1f\nDesviacion Estandar: %.1f\n", nota_max, nota_min, desvstd);
	}
	else{
		printf("\nCargue el archivo en la opcion 1.\n");
	}
}

void clasificarEstudiantes(char path[], estudiante curso[]){// Funcion que separa los alumnos con promedio sobre 4.0 y menor que 4.0
	FILE *aprobados;
	FILE *reprobados;
	int i;
	aprobados = fopen("Aprobados.txt", "w");
	reprobados= fopen("Desaprobados.txt", "w");
	for (i=0;i<3;i++){
		if(curso[i].prom>4.0){
			fprintf(aprobados, "%s %s %s \n", curso[i].nombre, curso[i].apellidoP, curso[i].apellidoM);
		}
		else{
			fprintf(reprobados,"%s %s %s \n", curso[i].nombre, curso[i].apellidoP, curso[i].apellidoM);
		}
	}
	fclose(reprobados);
	fclose(aprobados);
}

void menu(estudiante curso[]){
	int opcion;
    do{
		printf( "\n   1. Cargar Estudiantes" );
		printf( "\n   2. Ingresar notas" );
		printf( "\n   3. Mostrar Promedios" );
		printf( "\n   4. Almacenar en archivo" );
		printf( "\n   5. Clasificar Estudiantes " );
		printf( "\n   6. Salir." );
		printf( "\n\n   Introduzca opción (1-6): " );
		scanf( "%d", &opcion );
        switch ( opcion ){
			case 1: cargarEstudiantes("estudiantes.txt", curso); //carga en lote una lista en un arreglo de registros
					break;

			case 2:  registroCurso(curso);// Realiza ingreso de notas en el registro curso 
					break;

			case 3: metricasEstudiantes(curso); //presenta métricas de disperción, tales como mejor promedio; más bajo; desviación estándard.
					break;

			case 4: grabarEstudiantes("test.txt",curso);
					break;
            case 5: clasificarEstudiantes("destino", curso); // clasi
            		break;
         }

    } while ( opcion != 6 );
}

int main(){
	estudiante curso[30]; 
	menu(curso); 
	return EXIT_SUCCESS; 
}
